<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

// contoh tabel
Route::get('/table', function () {
    return view('contoh_tabel.table');
});
Route::get('/datatable', function () {
    return view('contoh_tabel.datatable');
});

// genre
Route::get('/genre', 'C_genre@index');
Route::get('/genre/tambah', 'C_genre@tambah');
Route::post('/genre/tambahsubmit', 'C_genre@tambahSubmit');
Route::get('/genre/{genre_id}', 'C_genre@lihat');
Route::get('/genre/{genre_id}/edit', 'C_genre@edit');
Route::put('/genre/{genre_id}', 'C_genre@editSubmit');
Route::delete('/genre/{genre_id}', 'C_genre@hapusSubmit');

// cast
Route::get('/cast', 'C_cast@index');
Route::get('/cast/tambah', 'C_cast@tambah');
Route::post('/cast/tambahsubmit', 'C_cast@tambahSubmit');
Route::get('/cast/{cast_id}', 'C_cast@lihat');
Route::get('/cast/{cast_id}/edit', 'C_cast@edit');
Route::put('/cast/{cast_id}', 'C_cast@editSubmit');
Route::delete('/cast/{cast_id}', 'C_cast@hapusSubmit');
