@extends('layout.master')

@section('title')
  Edit Genre
@endsection

@section('content')
<div>
  <form action="/cast/{{$data->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <div class="form-group">
        <label for="title">Nama</label>
        <input type="text" class="form-control" name="nama" id="nama" value="{{$data->nama}}" placeholder="Nama Cast">
        @error('nama')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
        @enderror
        <label for="title">Umur</label>
        <input type="text" class="form-control" name="umur" id="umur" value="{{$data->umur}}" placeholder="Umur">
        @error('umur')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
        @enderror
        <label for="title">Bio</label>
        <input type="text" class="form-control" name="bio" id="bio" value="{{$data->bio}}" placeholder="Bio">
        @error('bio')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
  </form>
</div>
@endsection