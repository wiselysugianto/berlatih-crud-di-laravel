@extends('layout.master')

@section('title')
  Tambah Cast
@endsection

@section('content')
<div>
  <form action="/cast/tambahsubmit" method="POST">
    @csrf
    <div class="form-group">
      <label for="title">Nama</label>
      <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Cast">
      @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
      @enderror
      <label for="title">Umur</label>
      <input type="text" class="form-control" name="umur" id="umur" placeholder="Umur">
      @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
      @enderror
      <label for="title">Bio</label>
      <input type="text" class="form-control" name="bio" id="bio" placeholder="Bio">
      @error('bio')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
  </form>
</div>
@endsection