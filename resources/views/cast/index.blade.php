@extends('layout.master')

@section('title')
  List Cast
@endsection

@section('content')
<a href="/cast/tambah" class="btn btn-primary my-3">Tambah</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($data as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->umur}}</td>
                <td>{{$value->bio}}</td>
                <td>
                  <form action="/cast/{{$value->id}}" method="POST">
                    <a href="/cast/{{$value->id}}" class="btn btn-info">Lihat</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                      @csrf
                      @method('DELETE')
                      <input type="submit" class="btn btn-danger my-1" value="Hapus">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>Tidak ada data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection