@extends('layout.master')

@section('title')
  Tambah Genre
@endsection

@section('content')
<div>
  <form action="/genre/tambahsubmit" method="POST">
    @csrf
    <div class="form-group">
      <label for="title">Nama</label>
      <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Genre">
      @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
  </form>
</div>
@endsection