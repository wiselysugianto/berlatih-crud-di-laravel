@extends('layout.master')

@section('title')
  Edit Genre
@endsection

@section('content')
<div>
  <form action="/genre/{{$data->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label for="title">Nama</label>
      <input type="text" class="form-control" name="nama" id="nama" value="{{$data->nama}}" placeholder="Nama Genre">
      @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
  </form>
</div>
@endsection