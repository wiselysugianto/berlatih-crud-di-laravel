<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class C_genre extends Controller
{
	public function index()
	{
		$data = DB::table('genre')->get();
		return view('genre.index', compact('data'));
	}

	public function tambah()
	{
		return view('genre.tambah');
	}

	public function tambahSubmit(Request $request)
	{
		$request->validate([
			'nama' => 'required|max:20|unique:genre',
		]);
		$query = DB::table('genre')->insert([
			'nama' => $request['nama'],
		]);
		return redirect('/genre');
	}

	public function lihat($genre_id)
	{
		$data = DB::table('genre')->where('id', $genre_id)->first();
		return view('genre.lihat', compact('data'));
	}

	public function edit($genre_id)
	{
		$data = DB::table('genre')->where('id', $genre_id)->first();
		return view('genre.edit', compact('data'));
	}

	public function editSubmit(Request $request, $genre_id)
	{
		$request->validate([
			'nama' => 'required|max:20',
		]);
		$query = DB::table('genre')->where('id', $genre_id)->update([
			'nama' => $request['nama'],
		]);
		return redirect('/genre');
	}

	public function hapusSubmit($genre_id)
	{
		$query = DB::table('genre')->where('id', $genre_id)->delete();
		return redirect('/genre');
	}
}
