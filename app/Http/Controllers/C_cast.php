<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class C_cast extends Controller
{
    public function index()
    {
        $data = DB::table('cast')->get();
        return view('cast.index', compact('data'));
    }

    public function tambah()
    {
        return view('cast.tambah');
    }

    public function tambahSubmit(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:20',
            'umur' => 'required|numeric',
            'bio'  => 'required',
        ]);
        $query = DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio'  => $request['bio'],
        ]);
        return redirect('/cast');
    }

    public function lihat($cast_id)
    {
        $data = DB::table('cast')->where('id', $cast_id)->first();
        return view('cast.lihat', compact('data'));
    }

    public function edit($cast_id)
    {
        $data = DB::table('cast')->where('id', $cast_id)->first();
        return view('cast.edit', compact('data'));
    }

    public function editSubmit(Request $request, $cast_id)
    {
        $request->validate([
            'nama' => 'required|max:20',
            'umur' => 'required|numeric',
            'bio'  => 'required',
        ]);
        $query = DB::table('cast')->where('id', $cast_id)->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio'  => $request['bio'],
        ]);
        return redirect('/cast');
    }

    public function hapusSubmit($cast_id)
    {
        $query = DB::table('cast')->where('id', $cast_id)->delete();
        return redirect('/cast');
    }
}
